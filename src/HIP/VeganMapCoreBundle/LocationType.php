<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle;

interface LocationType {
    const RESTAURANT = 0;
    const CAFE = 1;
    const STORE = 2;
}