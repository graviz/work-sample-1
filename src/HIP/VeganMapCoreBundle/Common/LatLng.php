<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Common;

class LatLng {

    public $lat;
    public $lng;

    public function __construct($lat, $lng) {
        $this->lat = $lat;
        $this->lng = $lng;
    }

}