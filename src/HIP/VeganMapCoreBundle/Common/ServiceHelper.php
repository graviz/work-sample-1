<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Common;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ServiceHelper {

    /**
     * @return \Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine
     */
    public static function templating() {
        return self::container()->get('templating');
    }

    /**
     * @param $key
     * @return string
     */
    public static function parameter($key) {
        return self::container()->getParameter($key);
    }

    /**
     * @return \Symfony\Component\HttpKernel\Config\FileLocator
     */
    public static function locator() {
        return self::container()->get('file_locator');
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public static function em() {
        return self::container()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return \Symfony\Component\HttpKernel\KernelInterface
     */
    public static function kernel() {
        return self::container()->get('kernel');
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    public static function trans() {
        return self::container()->get('translator');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public static function request() {
        return self::container()->get('request');
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    public static function router() {
        return self::container()->get('router');
    }

    /**
     * @return \Symfony\Component\Form\FormFactory
     */
    public static function formFactory() {
        return self::container()->get('form.factory');
    }

    /**
     * @return \Symfony\Bridge\Monolog\Logger
     */
    public static function logger() {
        return self::container()->get('logger');
    }

    /**
     * @return \Swift_Mailer
     */
    public static function mailer() {
        return self::container()->get('mailer');
    }

    /**
     * @return ContainerInterface
     */
    private static function container() {
        global $kernel;

        if ('AppCache' == get_class($kernel))
            $kernel = $kernel->getKernel();

        return $kernel->getContainer();
    }

}