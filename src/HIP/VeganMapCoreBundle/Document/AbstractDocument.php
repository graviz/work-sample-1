<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class AbstractDocument
 * @package HIP\VeganMapCoreBundle\Document
 * @ODM\MappedSuperclass
 */
abstract class AbstractDocument {

    /** @ODM\Id */
    protected $id;
    const ID = 'id';

    /**
     * @param string $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

}
