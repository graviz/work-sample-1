<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class AdminUser
 * @package HIP\VeganMapCoreBundle\Document
 * @ODM\Document(collection="vm_admin_users")
 */
class AdminUser extends BaseUser {
    /**
     * @ODM\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var $role Role
     * @ODM\ReferenceOne(targetDocument="HIP\VeganMapCoreBundle\Document\Role")
     */
    protected $role;


    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRole() {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role) {
        $this->role = $role;
    }

    public function getRoles() {
        return array($this->role->getRole());
    }

}