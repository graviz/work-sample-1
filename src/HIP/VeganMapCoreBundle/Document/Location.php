<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class Location
 * @package HIP\VeganMapCoreBundle\Document
 * @ODM\Document(collection="vm_locations")
 */
class Location extends AbstractDocument {

    /**
     * @var string
     * @ODM\String
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @var string
     * @ODM\String
     */
    protected $description;

    /**
     * @var string
     * @ODM\String
     */
    protected $address;

    /**
     * @var string
     * @ODM\Float
     * @Assert\NotBlank
     */
    protected $latitude;

    /**
     * @var string
     * @ODM\Float
     * @Assert\NotBlank
     */
    protected $longitude;

    /**
     * @var string
     * @ODM\String
     */
    protected $telephone;

    /**
     * @var string
     * @ODM\String
     * @Assert\Url(protocols={"http", "https"})
     */
    protected $url;

    /**
     * See FoodType
     * @var int
     * @ODM\Int
     */
    protected $foodType;

    /**
     * See LocationType
     * @var int
     * @ODM\Int
     */
    protected $locationType;

    /**
     * @var OpeningHour[]
     * @ODM\EmbedMany(targetDocument="HIP\VeganMapCoreBundle\Document\OpeningHour")
     */
    protected $openingHours = [];

    /**
     * @var Image
     * @ODM\ReferenceMany(targetDocument="HIP\VeganMapCoreBundle\Document\Image", cascade="all")
     */
    protected $images = [];

    public function __construct() {
        for ($i = 0; $i < 7; $i++) {
            $hour = new OpeningHour();
            $hour->setDay($i);
            $this->openingHours[] = $hour;
        }
    }

    /**
     * @return string
     */
    public function prettyUrl() {
        $patterns = ['http://www.', 'https://www.', 'http://', 'https://', 'www.'];
        foreach ($patterns as $pattern)
            if (strpos($this->url, $pattern) === 0)
                return substr(str_replace($pattern, '', $this->url), 0, 50);
        return $this->url;
    }

    /**
     * Returns 1 if open, 0 if closed and -1 if not known.
     * @return int
     */
    public function isOpen() {
        if (count($this->openingHours) == 0)
            return -1;

        $dayOfWeek = intval(date('N')) - 1;

        foreach ($this->openingHours as $openingHour) {
            if ($openingHour->getOpens() == '' || $openingHour->getCloses() == '')
                continue;

            if ($openingHour->getDay() == $dayOfWeek) {
                $opensParsed = date_parse($openingHour->getOpens());
                $closesParsed = date_parse($openingHour->getCloses());

                $opens = new DateTime();
                $opens->setTime($opensParsed['minute'], $opensParsed['second'], 0);

                $closes = new DateTime();
                $closes->setTime($closesParsed['minute'], $closesParsed['second'], 0);

                $now = new DateTime();

                if ($now->getTimestamp() < $closes->getTimestamp() && $now->getTimestamp() > $opens->getTimestamp())
                    return 1;

                return 0;
            }
        }

        return -1;
    }


    //--------------------------------------------

    /**
     * @return string
     * @JSON\JsonProperty(name="name", type="string")
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @JSON\JsonProperty(name="name", type="string")
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="address", type="string")
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * @param string $addressField
     * @JSON\JsonProperty(name="address", type="string")
     */
    public function setAddress($addressField) {
        $this->address = $addressField;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="description", type="string")
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     * @JSON\JsonProperty(name="description", type="string")
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="foodType", type="int")
     */
    public function getFoodType() {
        return $this->foodType;
    }

    /**
     * @param int $foodType
     * @JSON\JsonProperty(name="foodType", type="int")
     */
    public function setFoodType($foodType) {
        $this->foodType = $foodType;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="locationType", type="int")
     */
    public function getLocationType() {
        return $this->locationType;
    }

    /**
     * @param int $locationType
     * @JSON\JsonProperty(name="locationType", type="int")
     */
    public function setLocationType($locationType) {
        $this->locationType = $locationType;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="latitude", type="float")
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     * @JSON\JsonProperty(name="latitude", type="float")
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="longitude", type="float")
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     * @JSON\JsonProperty(name="longitude", type="float")
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    /**
     * @return OpeningHour[]
     * @JSON\JsonProperty(name="openingHours", type="HIP\VeganMapCoreBundle\Document\OpeningHour[]")
     */
    public function getOpeningHours() {
        return $this->openingHours;
    }

    /**
     * @param OpeningHour[] $openingHours
     * @JSON\JsonProperty(name="openingHours", type="HIP\VeganMapCoreBundle\Document\OpeningHour[]")
     */
    public function setOpeningHours($openingHours) {
        $this->openingHours = $openingHours;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="telephone", type="string")
     */
    public function getTelephone() {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @JSON\JsonProperty(name="telephone", type="string")
     */
    public function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="url", type="string")
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param string $url
     * @JSON\JsonProperty(name="url", type="string")
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @return Image[]
     */
    public function getImages() {
        return $this->images;
    }

    /**
     * @param mixed $image
     */
    public function setImages($image) {
        $this->images = $image;
    }

}