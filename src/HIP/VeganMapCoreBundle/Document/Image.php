<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="vm_location_images") */
class Image extends GridFSFile {
}