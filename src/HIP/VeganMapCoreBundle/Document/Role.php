<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * @ODM\Document(collection="vm_user_roles")
 */
class Role extends AbstractDocument implements RoleInterface {

    const ADMIN = 'ROLE_ADMIN';
    const USER = 'ROLE_USER';

    const Role = 'role';

    /**
     * @ODM\String()
     */
    protected $name;

    /**
     * @ODM\String()
     */
    protected $role;

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Returns the role.
     *
     * This method returns a string representation whenever possible.
     *
     * When the role cannot be represented with sufficient precision by a
     * string, it should return null.
     *
     * @return string|null A string representation of the role, or null
     */
    public function getRole() {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role) {
        $this->role = $role;
    }

}