<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class LocationMarker
 *
 * Reduced form of of a Location stored in the database.
 * Required information to display a marker on google map.
 *
 * @package HIP\VeganMapCoreBundle\Document
 * @ODM\Document(collection="vm_locations")
 */
class LocationMarker {

    /**
     * @var string
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string
     * @ODM\String
     */
    private $name;

    /**
     * @var string
     * @ODM\Float
     */
    private $latitude;

    /**
     * @var string
     * @ODM\Float
     */
    private $longitude;

    /**
     * See FoodType
     * @var int
     * @ODM\Int
     */
    private $foodType;

    /**
     * See LocationType
     * @var int
     * @ODM\Int
     */
    private $locationType;

    /**
     * @param string $id
     * @JSON\JsonProperty(name="id", type="string")
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="id", type="string")
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="name", type="string")
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @JSON\JsonProperty(name="name", type="string")
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="foodType", type="int")
     */
    public function getFoodType() {
        return $this->foodType;
    }

    /**
     * @param int $foodType
     * @JSON\JsonProperty(name="foodType", type="int")
     */
    public function setFoodType($foodType) {
        $this->foodType = $foodType;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="locationType", type="int")
     */
    public function getLocationType() {
        return $this->locationType;
    }

    /**
     * @param int $locationType
     * @JSON\JsonProperty(name="locationType", type="int")
     */
    public function setLocationType($locationType) {
        $this->locationType = $locationType;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="latitude", type="float")
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     * @JSON\JsonProperty(name="latitude", type="float")
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="longitude", type="float")
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     * @JSON\JsonProperty(name="longitude", type="float")
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

}