<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class OpeningHour
 * @package HIP\VeganMapCoreBundle\Document
 * @ODM\EmbeddedDocument
 */
class OpeningHour {

    /**
     * Days of the week. Starting with 0 = Monday.
     * @var int
     * @ODM\Int
     * @Assert\Range(min="0", max="6")
     */
    private $day;

    /**
     * @var string
     * @ODM\String
     */
    private $opens;

    /**
     * @var string
     * @ODM\String
     */
    private $closes;

    /**
     * @param int $day
     * @JSON\JsonProperty(name="day", type="int")
     */
    public function setDay($day) {
        $this->day = $day;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="day", type="int")
     */
    public function getDay() {
        return $this->day;
    }

    /**
     * @param string $opens
     * @JSON\JsonProperty(name="opens", type="string")
     */
    public function setOpens($opens) {
        $this->opens = $opens;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="opens", type="string")
     */
    public function getOpens() {
        return $this->opens;
    }

    /**
     * @param string $closes
     * @JSON\JsonProperty(name="closes", type="string")
     */
    public function setCloses($closes) {
        $this->closes = $closes;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="closes", type="string")
     */
    public function getCloses() {
        return $this->closes;
    }
} 