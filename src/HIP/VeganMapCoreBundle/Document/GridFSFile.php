<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use MongoGridFSFile;

/**
 * @ODM\MappedSuperclass
 * Represents a GridFS Document.
 */
abstract class GridFSFile extends AbstractDocument {

    /** @ODM\Field */
    protected $name;

    /** @ODM\File */
    protected $file;

    /** @ODM\String */
    protected $filename;

    /** @ODM\Field */
    protected $uploadDate;

    /** @ODM\Field */
    protected $length;

    /** @ODM\Field */
    protected $chunkSize;

    /** @ODM\Field */
    protected $md5;

    /** @ODM\String */
    protected $contentType;

    /** @param string $name */
    public function setName($name) {
        $this->name = $name;
    }

    /** @return string */
    public function getName() {
        return $this->name;
    }

    /** @return MongoGridFSFile */
    public function getFile() {
        return $this->file;
    }

    /** @param $file */
    public function setFile($file) {
        $this->file = $file;
    }

    /** @return mixed */
    public function getLength() {
        return $this->length;
    }

    /** @param mixed $length */
    public function setLength($length) {
        $this->length = $length;
    }

    /** @return mixed */
    public function getUploadDate() {
        return $this->uploadDate;
    }

    /** @param mixed $uploadDate */
    public function setUploadDate($uploadDate) {
        $this->uploadDate = $uploadDate;
    }

    /** @return mixed */
    public function getMd5() {
        return $this->md5;
    }

    /** @param mixed $md5 */
    public function setMd5($md5) {
        $this->md5 = $md5;
    }

    /** @return mixed */
    public function getChunkSize() {
        return $this->chunkSize;
    }

    /** @param mixed $chunkSize */
    public function setChunkSize($chunkSize) {
        $this->chunkSize = $chunkSize;
    }

    /** @return string */
    public function getFilename() {
        return $this->filename;
    }

    /** @param string $filename */
    public function setFilename($filename) {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getContentType() {
        return $this->contentType;
    }

    /**
     * @param mixed $contentType
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;
    }
}