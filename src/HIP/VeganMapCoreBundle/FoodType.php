<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapCoreBundle;

interface FoodType {
    const VEGAN = 0;
    const VEGETARIAN = 1;
    const OMNIVORE = 2;
}