<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapAdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class DashboardController
 * @package HIP\VeganMapCoreBundle\Controller
 * @Sensio\Route("/", host="admin.%base_host%")
 */
class DashboardController extends AbstractController {

    /**
     * @Sensio\Route("/", name="admin_index")
     * @Sensio\Method("GET")
     * @Sensio\Template
     */
    public function indexAction() {
        return [];
    }

}