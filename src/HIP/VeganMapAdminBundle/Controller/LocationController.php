<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapAdminBundle\Controller;

use Doctrine\ORM\PersistentCollection;
use HIP\VeganMapCoreBundle\Document\GridFSFile;
use HIP\VeganMapCoreBundle\Document\Image;
use HIP\VeganMapCoreBundle\Document\Location;
use HIP\VeganMapAdminBundle\Form\LocationFormType;
use HIP\VeganMapCoreBundle\Document\LocationMarker;
use HIP\VeganMapCoreBundle\FoodType;
use HIP\VeganMapCoreBundle\LocationType;
use Imagick;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Weasel\WeaselDoctrineAnnotationDrivenFactory;

/**
 * @Sensio\Route("/location", host="admin.%base_host%")
 */
class LocationController extends AbstractController {

    /**
     * @Sensio\Route("/", name="location_index")
     * @Sensio\Method("GET")
     * @Sensio\Template
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request) {
        $locations = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->findAll();
        return ['locations' => $locations,];
    }

    /**
     * Returns an image from grid fs. Image can be scaled by width and height params.
     *
     * @Sensio\Route("/{locationId}/thumbnail/{imageId}/{width}x{height}", name="location_image_thumbnail", requirements={"width"="\d+", "height"="\d+"})
     * @Sensio\Method("GET")
     *
     * @param $locationId
     * @param $imageId
     * @param $width
     * @param $height
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function imageAction($locationId, $imageId, $width, $height) {
        $location = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->find($locationId);

        if ($location == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        foreach ($location->getImages() as $image)
            if ($image->getId() == $imageId) {
                $imageData = FileHandler::cropImage($image, $width, $height);
                return new Response($imageData, Response::HTTP_OK, [
                    'Content-Type' => $image->getContentType()
                ]);
            }

        return new Response('', Response::HTTP_NOT_FOUND);
    }

    /**
     * Handles the submission of a location form.
     *
     * @Sensio\Route("/ajax/form/{id}", defaults={"id"=null}, name="location_ajax_form", options={"expose"=true})
     * @Sensio\Method("GET")
     *
     * @param null $id
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @return JsonResponse
     */
    public function ajaxLocationFormAction($id = null) {
        if ($id == null)
            $location = new Location();
        else
            $location = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->find($id);

        if ($location == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        $form = $this->locationForm($location);

        if ($id == null)
            $title = $this->get('translator')->trans('location.create');
        else
            $title = $this->get('translator')->trans('location.edit');

        $body = $this->renderView('HIPVeganMapAdminBundle:Location:location_form.html.twig', ['location_form' => $form->createView()]);
        $footer = $this->renderView('HIPVeganMapAdminBundle:Location:location_form_footer.html.twig');

        return new JsonResponse([
            'title' => $title,
            'body' => $body,
            'footer' => $footer,
        ]);
    }

    /**
     * Returns details about a location. Bootstrap Modal Dialogs are used for that.
     * Returns the title, body and footer of the dialog.
     *
     * @Sensio\Route("/ajax/detail/{id}", name="location_ajax_detail", options={"expose"=true})
     * @Sensio\Method("GET")
     *
     * @param string $id
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @return JsonResponse
     */
    public function ajaxLocationDetailAction($id) {
        /** @var Location $location */
        $location = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->find($id);

        if ($location == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        $imageSrc = '/bundles/hipveganmapadmin/images/';

        $ext = ".png";

        $foodType = "";
        $locationType = "";

        switch ($location->getLocationType()) {
            case LocationType::CAFE:
                $locationType = "cafe";
                break;
            case LocationType::STORE:
                $locationType = "store";
                break;
            case LocationType::RESTAURANT:
                $locationType = "restaurant";
                break;
        }

        switch ($location->getFoodType()) {
            case FoodType::VEGETARIAN:
                $foodType = "vegetarian";
                break;
            case FoodType::OMNIVORE:
                $foodType = "mixed";
                break;
            case FoodType::VEGAN:
                $foodType = "vegan";
                break;
        }

        $imageSrc .= $locationType . '_' . $foodType . $ext;

        $openingHourMap = $this->prettyOpeningHours($location);

        $title = $location->getName();
        $body = $this->renderView('HIPVeganMapAdminBundle:Location:location_detail.html.twig', [
            'location' => $location,
            'image' => $imageSrc,
            'openingHourMap' => $openingHourMap,
        ]);
        $footer = $this->renderView('HIPVeganMapAdminBundle:Location:location_detail_footer.html.twig');

        return new JsonResponse([
            'title' => $title,
            'body' => $body,
            'footer' => $footer,
        ]);
    }

    /**
     * Deletes a location.
     *
     * @Sensio\Route("/ajax/delete/{id}", name="location_ajax_delete", options={"expose"=true})
     * @Sensio\Method("GET")
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajaxDeleteAction($id) {
        $location = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->find($id);

        if ($location != null) {
            $this->dm()->remove($location);
            $this->dm()->flush();
            return new Response();
        }

        return new Response('', Response::HTTP_NOT_FOUND);
    }

    /**
     * Saves a new or existing location.
     *
     * @Sensio\Route("/ajax/save/{id}", defaults={"id"=null}, name="location_ajax_save", options={"expose"=true})
     * @Sensio\Method("POST")
     *
     * @param Request $request
     * @param string $id
     * @return array
     */
    public function ajaxSaveAction(Request $request, $id = null) {
        $location = null;

        if ($id != null)
            $location = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->find($id);

        if ($id != null && $location == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        if ($location == null)
            $location = new Location();

        $form = $this->locationForm($location);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->dm()->persist($location);
            $this->dm()->flush();
            return new JsonResponse(['ok' => true]);
        }

        $title = $this->get('translator')->trans('location.create');
        $body = $this->renderView('HIPVeganMapAdminBundle:Location:location_form.html.twig', ['location_form' => $form->createView()]);
        $footer = $this->renderView('HIPVeganMapAdminBundle:Location:location_form_footer.html.twig');

        return new JsonResponse([
            'title' => $title,
            'body' => $body,
            'footer' => $footer,
        ]);
    }

    /**
     * Returns a rendered table with all locations
     *
     * @Sensio\Route("/ajax/table", name="location_ajax_table", options={"expose"=true})
     * @Sensio\Method("GET")
     * @Sensio\Template
     */
    public function ajaxTableAction() {
        $locations = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->findAll();
        return ['locations' => $locations,];
    }

    /**
     * Returns a json string with all locations.
     *
     * @Sensio\Route("/ajax/json", name="location_ajax_json", options={"expose"=true})
     * @Sensio\Method("GET")
     *
     * @return Response
     */
    public function ajaxJsonAction() {
        $factory = new WeaselDoctrineAnnotationDrivenFactory();
        $mapper = $factory->getJsonMapperInstance();

        $locations = $this->dm()->getRepository('HIPVeganMapCoreBundle:LocationMarker')->findAll();

        $jsonString = $mapper->writeString($locations, get_class(new LocationMarker()) . '[]');

        return new Response(
            $jsonString,
            Response::HTTP_OK,
            ['Content-Type' => 'application/json']
        );
    }

    /**
     * Uploads an image to a location.
     *
     * @Sensio\Route("/ajax/upload/image", name="location_ajax_image_upload", options={"expose"=true})
     * @Sensio\Method("POST")
     */
    public function ajaxUploadAction(Request $request) {
        /** @var $upload \Symfony\Component\HttpFoundation\File\UploadedFile */
        $upload = $request->files->get('file');

        $jsonData = json_decode($request->request->get('json'), true);
        $locationId = $jsonData['location'];

        /** @var Location $location */
        $location = $this->dm()->getRepository('HIPVeganMapCoreBundle:Location')->find($locationId);

        if ($location == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        if (FileHandler::isImage($upload)) {
            $image = FileHandler::processImage($upload);

            $location->getImages()[] = $image;

            $this->dm()->persist($image);
            $this->dm()->flush();
        } else {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        return new JsonResponse(['success' => true]);
    }

    // --------------------------------------------------------

    private function locationForm(Location $location) {
        return $this->createForm(
            new LocationFormType(),
            $location,
            ['action' => $this->generateUrl('location_ajax_save'), 'method' => 'POST']
        );
    }

    /**
     * Returns an array with the format:
     * [
     *      [
     *          'startDay' => ...
     *          'endDay' => ...
     *          'opens' => ...
     *          'closes' => ...
     *      ],
     *      ...
     * ]
     * @param Location $location
     * @return array
     */
    private function prettyOpeningHours(Location $location) {
        $hourMap = [];
        $index = 0;
        foreach ($location->getOpeningHours() as $openingHour) {
            if (count($hourMap) == 0) {
                $hourMap[$index] = [
                    'startDay' => $openingHour->getDay(),
                    'endDay' => $openingHour->getDay(),
                    'opens' => $openingHour->getOpens(),
                    'closes' => $openingHour->getCloses(),
                ];
            } else {
                $prevDay = $hourMap[$index];
                if (isset($prevDay) && $prevDay['opens'] == $openingHour->getOpens() && $prevDay['closes'] == $openingHour->getCloses()) {
                    $hourMap[$index]['endDay'] = $openingHour->getDay();
                } else {
                    $index++;
                    $hourMap[$index] = [
                        'startDay' => $openingHour->getDay(),
                        'endDay' => $openingHour->getDay(),
                        'opens' => $openingHour->getOpens(),
                        'closes' => $openingHour->getCloses(),
                    ];
                }
            }
        }

        return $hourMap;
    }
}

class FileHandler {

    public static $supportedImages = ['image/jpeg', 'image/jpg', 'image/png'];

    const IMAGE_MAX_HEIGHT = 1200;
    const IMAGE_MAX_WIDTH = 1200;

    /**
     * @param UploadedFile $file
     * @return Image
     */
    public static function processImage(UploadedFile $file) {
        $imagick = new Imagick($file->getPathname());

        if ($imagick->getImageHeight() > self::IMAGE_MAX_HEIGHT || $imagick->getImageWidth() > self::IMAGE_MAX_WIDTH) {
            $imagick->adaptiveResizeImage(self::IMAGE_MAX_WIDTH, self::IMAGE_MAX_HEIGHT, true);
            $imagick->writeImage($file->getPathname());
        }

        $image = new Image();
        self::createGrid($file, $image);

        return $image;
    }

    public static function resizeImage(Image $image, $width, $height, $bestFit = true) {
        $imagick = new Imagick();
        $imagick->readImageBlob($image->getFile()->getBytes());

        $imagick->adaptiveResizeImage($width, $height, $bestFit);

        return $imagick->getImageBlob();
    }

    public static function cropImage(Image $image, $width, $height) {
        $imagick = new Imagick();
        $imagick->readImageBlob($image->getFile()->getBytes());

        $imagick->cropThumbnailImage($width, $height);

        return $imagick->getImageBlob();
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public static function isImage(UploadedFile $file) {
        return in_array($file->getClientMimeType(), self::$supportedImages);
    }

    /**
     * @param UploadedFile $file
     * @param GridFSFile $grid
     */
    private static function createGrid(UploadedFile $file, GridFSFile &$grid) {
        $grid->setFile($file->getPathname());
        $grid->setFilename($file->getClientOriginalName());
        $grid->setContentType($file->getClientMimeType());
    }

}