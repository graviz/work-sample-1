<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapAdminBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AbstractController extends Controller {

    /**
     * @return DocumentManager
     */
    public function dm() {
        return $this->get('doctrine_mongodb')->getManager();
    }

}