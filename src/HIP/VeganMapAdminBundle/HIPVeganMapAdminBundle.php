<?php

namespace HIP\VeganMapAdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HIPVeganMapAdminBundle extends Bundle {

    public function getParent() {
        return 'FOSUserBundle';
    }

}