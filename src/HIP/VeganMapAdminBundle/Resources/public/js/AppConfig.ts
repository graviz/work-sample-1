/// <reference path="App.ts" />

require.config({
    baseUrl: "/bundles/hipveganmapadmin/js"
});

require(['App'],
    (main) => {
        $(document).ready(() => {
            new main.App();
        });
    }
);