/// <reference path="../modules/references.d.ts" />

import loc = require("src/location");
import net = require("src/net");
import file = require("src/file");
import dialog = require("src/dialog");

export class DashboardController {

    private googleMap:google.maps.Map;

    public index():void {
        var map = $(".google_map");

        this.googleMap = new google.maps.Map(map[0], {
            zoom: 4,
            center: new google.maps.LatLng(49.1746312, 10.926152)
        });

        this.loadMarkers();
    }

    private loadMarkers():void {
        var url = Routing.generate(loc.LOCATION_AJAX_JSON_ROUTE);
        net.Ajax.getJson(url, (response:loc.LocationMarker[])=> {
            response.forEach((location:loc.LocationMarker)=> {
                this.createMarker(location);
            });
        });
    }

    private createMarker(location:loc.LocationMarker):google.maps.Marker {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.latitude, location.longitude),
            map: this.googleMap,
            title: location.name,
            icon: file.LocationMarkerHelper.icon(location)
        });

        google.maps.event.addListener(marker, 'click', () => {
            dialog.LocationDialog.detail(location.id);
        });

        return marker;
    }

}