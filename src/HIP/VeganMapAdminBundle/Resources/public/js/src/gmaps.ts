/// <reference path="../modules/references.d.ts" />

export class GMapHelper {

    private static geocoder:google.maps.Geocoder = new google.maps.Geocoder();

    public static geocode(request:google.maps.GeocoderRequest,
                          callback:(results:google.maps.GeocoderResult[])=>void,
                          error?:(status:google.maps.GeocoderStatus)=>void) {
        GMapHelper.geocoder.geocode(request, (results:google.maps.GeocoderResult[], status:google.maps.GeocoderStatus) => {
                if (status == google.maps.GeocoderStatus.OK || status == google.maps.GeocoderStatus.ZERO_RESULTS) {
                    callback(results);
                } else {
                    alert("Oh Snap! An error ocured: " + status);
                    error(status);
                }
            }
        );
    }

    public static setMarker(LatLng:google.maps.LatLng, googleMap:google.maps.Map, title:string):void {
        new google.maps.Marker({
            position: LatLng,
            map: googleMap,
            title: title,
            draggable: false
        });
    }
}
