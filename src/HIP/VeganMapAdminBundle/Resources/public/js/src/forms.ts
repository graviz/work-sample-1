/// <reference path="../modules/references.d.ts" />

import net = require("src/net");
import gmaps = require("src/gmaps");
import location = require("src/location");
import dialog = require("src/dialog");
import model = require("src/model");

export class LocationForm {

    private geocoderField:JQuery;
    private geocoderTimer = null;
    private geocoderResultList:JQuery;
    private locationModel:model.DataModel;

    private googleMap:google.maps.Map;
    private positionMarker:google.maps.Marker;
    private LatLng:google.maps.LatLng;

    private static GEOCODER_INTERVAL:number = 400;
    private static MAX_GEOCODER_RESULTS:number = 5;

    private id:string;

    constructor(dataModel:model.DataModel) {
        this.locationModel = dataModel;
    }

    public create(id?:string):void {
        dialog.Helper.empty();
        dialog.Helper.open();

        this.id = id;

        var url = Routing.generate(location.LOCATION_AJAX_FORM_ROUTE, {id: this.id});

        net.Ajax.getJson(url, (response:location.DialogResponse) => {
            this.onDialogLoad(<dialog.DialogContent>response);
        });
    }

    private onDialogLoad(content:dialog.DialogContent):void {
        dialog.Helper.setContent(content);

        this.geocoderField = $("#geocoder");
        this.geocoderField.on("keyup", ()=> {
            this.geocodeSearch();
        });

        this.geocoderResultList = $("#geocoder-results");

        var zoom = 15;
        var setMarker = true;

        var latitude = $("#latitude").val();
        var longitude = $("#longitude").val();

        if (latitude == "" && longitude == ""){
            zoom = 2;
            setMarker = false;
        }

        latitude = latitude == "" ? 49.1746312 : latitude;
        longitude = longitude == "" ? 10.926152 : longitude;

        var center = new google.maps.LatLng(latitude, longitude);

        this.googleMap = new google.maps.Map($("#map_preview")[0], {
            zoom: zoom,
            center: center
        });

        if (setMarker)
            gmaps.GMapHelper.setMarker(center, this.googleMap, this.geocoderField.val());

        /**
         * This event is fired when the modal has been made visible to the user
         * (will wait for CSS transitions to complete). If caused by a click,
         * the clicked element is available as the relatedTarget property of the event.
         */
        dialog.Helper.dialog.on("shown.bs.modal", ()=> {
            google.maps.event.trigger(this.googleMap, "resize");
        });

        dialog.Helper.dialog.find("button.submit").click(()=> {
            this.submit();
        });
    }

    private submit():void {
        var url = Routing.generate(location.LOCATION_AJAX_SAVE_ROUTE, {id: this.id});
        var form = $("form[name=location]");
        var postData = form.serialize();

        dialog.Helper.empty();
        dialog.Helper.showLoading();

        net.Ajax.postForm(url, postData, (response:location.DialogResponse)=> {
            if (response.ok) {
                dialog.Helper.close();
                this.locationModel.reloadModel();
            } else {
                this.onDialogLoad(<dialog.DialogContent>response);
            }
        });
    }

    private geocodeSearch():void {
        if (this.geocoderTimer != null)
            clearTimeout(this.geocoderTimer);

        var address = this.geocoderField.val();
        address = address.split(' ').join('+');
        address = address.replace(/\n/g, '+');

        this.geocoderTimer = setTimeout(()=> {
            gmaps.GMapHelper.geocode({address: address}, (results:google.maps.GeocoderResult[]) => {
                    if (results.length == 0) {
                        this.geocoderResultList.hide();
                        return;
                    }

                    this.geocoderResultList.empty();
                    this.geocoderResultList.show();
                    for (var i = 0; i < Math.min(LocationForm.MAX_GEOCODER_RESULTS, results.length); i++) {
                        var result = results[i];
                        var element = $("<li class='result'>" + result.formatted_address + "</li>");
                        element.appendTo(this.geocoderResultList);

                        var geocoderWidget = new GeocoderDataWidget(result, element);
                        geocoderWidget.click((data:google.maps.GeocoderResult)=> {
                            this.handleGeocoderSelection(data);
                        });
                    }

                }
            );
        }, LocationForm.GEOCODER_INTERVAL);

    }

    private refreshMarker(title:string):void {
        if (this.positionMarker != null)
            this.positionMarker.setMap(null);

        this.positionMarker = new google.maps.Marker({
            position: this.LatLng,
            map: this.googleMap,
            title: title,
            draggable: true
        });

        google.maps.event.addListener(this.positionMarker, 'dragend', (event) => {
            this.markerDragend(event.latLng.lat(), event.latLng.lng());
        });

        $("#latitude").val(this.LatLng.lat());
        $("#longitude").val(this.LatLng.lng());
    }

    private markerDragend(lat, lng):void {
        this.LatLng = new google.maps.LatLng(lat, lng);

        gmaps.GMapHelper.geocode({location: this.LatLng}, (results:google.maps.GeocoderResult[]) => {
                if (results.length == 0)
                    return; //TODO:: error?
                var data = results[0];

                this.refreshMarker(data.formatted_address);
                this.geocoderField.val(data.formatted_address);
            }
        );

    }

    private handleGeocoderSelection(data:google.maps.GeocoderResult):void {
        this.geocoderResultList.hide();
        this.geocoderField.val(data.formatted_address);

        this.LatLng = new google.maps.LatLng(
            data.geometry.location.lat(),
            data.geometry.location.lng()
        );

        this.googleMap.setCenter(this.LatLng);

        this.refreshMarker(data.formatted_address);
    }

}

export class GeocoderDataWidget {

    private data:google.maps.GeocoderResult;
    private element:JQuery;

    constructor(data:google.maps.GeocoderResult, element:JQuery) {
        this.data = data;
        this.element = element;
    }

    get Data():google.maps.GeocoderResult {
        return this.data;
    }

    get Element():JQuery {
        return this.element;
    }

    public click(callback:(data:google.maps.GeocoderResult)=>void) {
        this.element.click(()=> {
            callback(this.Data);
        });
    }
}