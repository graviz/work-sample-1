/// <reference path="../modules/references.d.ts" />

import location = require("src/location");
import net = require("src/net");
import file_ = require("src/file");
import gmaps = require('src/gmaps');
import forms = require('src/forms');

export class LocationDialog {

    private static id:string;
    public static locationForm:forms.LocationForm;
    private static googleMap:google.maps.Map;

    public static detail(id:string):void {
        LocationDialog.id = id;
        Helper.open();

        var url = Routing.generate(location.LOCATION_AJAX_DETAIL_ROUTE, {id: id});

        net.Ajax.getJson(url, (response:location.DialogResponse)=> {
            LocationDialog.showLocationDetail(response, id);
        });
    }

    private static showLocationDetail(response:location.DialogResponse, id:string) {
        Helper.setContent(<DialogContent>response);
        Helper.hideLoading();

        $(".dialog-edit").click(()=> {
            LocationDialog.edit(id);
        });

        LocationDialog.loadMapPreview();

        var fileChooser = $("#locationImageChooser");

        fileChooser.change(() => {
            var files:File[] = fileChooser.prop('files');
            var images:File[] = [];
            for (var i = 0; i < files.length; i++)
                if (file_.FileManager.isImage(files[i]))
                    images.push(files[i]);

            fileChooser.replaceWith(fileChooser = fileChooser.clone(true));

            var url = Routing.generate(location.LOCATION_AJAX_IMAGE_UPLOAD);
            LocationDialog.uploadFiles(url, images);
        });
    }

    private static uploadFiles(url:string, files:File[]) {
        files.forEach((file:File)=> {
            file_.FileManager.uploadFile(
                url,
                file,
                {location: LocationDialog.id},
                (response:Object) => {
                    console.log(response);
                },
                (progress:number, loaded:number) => {
                    console.log(progress, loaded);
                }
            );
        });
    }

    /**
     * Geocodes the lat and long value of the location and displays a map marker and inits a google map.
     */
    private static loadMapPreview():void {
        var map = $(".google_map");
        var lat:number = map.data("lat");
        var lng:number = map.data("lng");
        var marker:number = map.data("marker");

        var LatLng:google.maps.LatLng = new google.maps.LatLng(lat, lng);

        LocationDialog.googleMap = new google.maps.Map(map[0], {
            zoom: 15,
            center: LatLng
        });

        Helper.dialog.on("shown.bs.modal", ()=> {
            var currentCenter = LocationDialog.googleMap.getCenter();
            google.maps.event.trigger(LocationDialog.googleMap, "resize");
            LocationDialog.googleMap.setCenter(currentCenter);
        });

        setTimeout(()=> {

        }, 100);

        gmaps.GMapHelper.geocode({location: LatLng}, (results:google.maps.GeocoderResult[]) => {
                if (results.length == 0)
                    return;

                var data = results[0];

                new google.maps.Marker({
                    position: LatLng,
                    map: LocationDialog.googleMap,
                    title: data.formatted_address,
                    draggable: false,
                    icon: marker
                });

                google.maps.event.trigger(LocationDialog.googleMap, "resize");
            }
        );

    }

    public static edit(id:string):void {
        LocationDialog.locationForm.create(id);
    }

}

export class Helper {

    public static dialog:JQuery;

    public static init():void {
        Helper.dialog = $("#bsDialog");

        /**
         * This event fires immediately when the show instance method is called.
         * If caused by a click, the clicked element is available as the
         * relatedTarget property of the event.
         */
        Helper.dialog.on("show.bs.modal", ()=> {
            Helper.showLoading();
        });

        /**
         * This event is fired when the modal has finished being hidden from the
         * user (will wait for CSS transitions to complete).
         */
        Helper.dialog.on("hidden.bs.modal", ()=> {
            Helper.empty();
        });
    }

    public static setContent(content:DialogContent):void {
        Helper.dialog.find(".ajax-loader").hide();
        Helper.dialog.find(".modal-label-placeholder").html(content.title);
        Helper.dialog.find(".modal-body-placeholder").html(content.body);
        Helper.dialog.find(".modal-footer").html(content.footer);
    }

    public static close():void {
        Helper.dialog.modal("hide");
    }

    public static open():void {
        Helper.dialog.modal("show");
    }

    public static empty():void {
        Helper.dialog.find(".modal-label-placeholder").empty();
        Helper.dialog.find(".modal-body-placeholder").empty();
        Helper.dialog.find(".modal-footer").empty();
    }

    public static showLoading():void {
        Helper.dialog.find(".ajax-loader").show();
    }

    public static hideLoading():void {
        Helper.dialog.find(".ajax-loader").hide();
    }

}
export interface DialogContent {
    title:string;
    body:string;
    footer:string;
}