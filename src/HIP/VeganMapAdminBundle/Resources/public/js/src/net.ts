/// <reference path="../modules/references.d.ts" />

/**
 * Wrapper class for the JQuery Ajax methods.
 */
export class Ajax {

    /**
     * Performs post request with a FormData object.
     * @param url
     * @param formData
     * @param onProgress
     * @param onSuccess
     */
    public static postFormData(url:string, formData:FormData, onProgress:(e:ProgressEvent)=>any, onSuccess:(response:string)=>any) {
        var request = new XMLHttpRequest();

        request.onload = function (e) {
            if (onSuccess) {
                var response = "";
                try {
                    response = JSON.parse((<any>(e.currentTarget)).response)
                } catch (e) {
                    console.log(e);
                }
                onSuccess(response);
            }

        };

        request.upload.onprogress = (e:ProgressEvent)=> {
            if (onProgress)
                onProgress(e);
        };

        request.open("POST", url);
        request.send(formData);
    }

    /**
     * Performs a POST request.
     * @param url
     * @param outputType
     * @param data
     * @param callback
     */
    public static post(url:string, outputType:string, data:Object, callback?:(response:any)=>any):void {
        new Ajax().post(url, outputType, data, callback);
    }

    public static postForm(url:string, data:any, callback?:(response:any)=>any):void {
        jQuery.post(url, data, callback, "json");
    }

    /**
     * Performs a POST request accepting json as response.
     * @param url
     * @param data
     * @param callback
     */
    public static postJson(url:string, data:Object, callback?:(response:any)=>any):void {
        new Ajax().postJson(url, data, callback);
    }

    /**
     * Performs a POST request accepting html as response.
     * @param url
     * @param data
     * @param callback
     */
    public static postHtml(url:string, data:Object, callback?:(response:any)=>any):void {
        new Ajax().postHtml(url, data, callback);
    }

    /**
     * Performs a GET request accepting json as response.
     * @param url
     * @param callback
     */
    public static getJson(url:string, callback?:(response:any) => any):void {
        new Ajax().getJson(url, callback);
    }

    /**
     * Performs a GET request accepting html as response.
     * @param url
     * @param callback
     */
    public static getHtml(url:string, callback?:(response:any) => void):void {
        new Ajax().getHtml(url, callback);
    }

    /**
     * The actual JQuery object.
     */
    private jqXHR:JQueryXHR;

    constructor() {
        /* Abort the current request if the browser window is closed. */
        jQuery(window).bind("beforeunload", () => {
            this.abort();
        });
    }

    /**
     * Performs a POST request.
     * @param url
     * @param outputType
     * @param data
     * @param callback
     */
    public post(url:string, outputType:string, data:any, callback?:(response:any)=>any):void {
        this.request(url, outputType, data, 'POST', callback);
    }

    /**
     * Performs a POST request accepting json as response.
     * @param url
     * @param data
     * @param callback
     */
    public postJson(url:string, data:any, callback?:(response:any)=>any):void {
        this.request(url, 'json', data, 'POST', callback);
    }

    /**
     * Performs a POST request accepting html as response.
     * @param url
     * @param data
     * @param callback
     */
    public postHtml(url:string, data:any, callback?:(response:any)=>any):void {
        this.request(url, 'html', data, 'POST', callback);
    }

    /**
     * Performs a GET request accepting json as response.
     * @param url
     * @param callback
     */
    public getJson(url:string, callback?:(response:any) => any):void {
        this.request(url, "json", null, "GET", callback);
    }

    /**
     * Performs a GET request accepting HTML as response.
     * @param url
     * @param callback
     */
    public getHtml(url:string, callback?:(response:any) => any):void {
        this.request(url, "html", null, "GET", callback);
    }

    /**
     * Performs an ajax request and aborts the current request.
     * @param url
     * @param outputType
     *      html,xml,json
     * @param data
     *      Any object will be stringified to a json string.
     * @param type
     *      GET,POST
     * @param callback
     */
    private request(url:string, outputType:string, data:Object, type:string, callback?:(response:any)=>any):void {

        this.abort();

        if (data != null)
            data = {json: JSON.stringify(data)};

        this.jqXHR = <JQueryXHR>$.ajax({
            url: url,
            dataType: outputType,
            async: true,
            type: type,
            data: data,
            success: function (output) {
                if (callback)
                    callback(output);
            }
        }).fail(function (jqXHR:JQueryXHR, textStatus:string, errorThrown:string) {
            if (textStatus == "abort")
                return;
            alert("textStatus: " + textStatus + "\n" + "errorThrown: " + errorThrown + "\n" + jqXHR.responseText);
        });
    }

    /**
     * Aborts the current ajax request.
     */
    private abort():void {
        if (this.jqXHR != null)
            this.jqXHR.abort();
    }
}