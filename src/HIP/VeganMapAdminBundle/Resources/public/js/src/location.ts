/// <reference path="../modules/references.d.ts" />

export interface DialogResponse {
    ok?:boolean;
    title:string;
    body:string;
    footer:string;
}

export class FoodType {
    public static VEGAN:number = 0;
    public static VEGETARIAN:number = 1;
    public static OMNIVORE:number = 2;
}

export class LocationType {
    public static  RESTAURANT:number = 0;
    public static  CAFE:number = 1;
    public static  STORE:number = 2;
}

export var LOCATION_AJAX_DETAIL_ROUTE = "location_ajax_detail";
export var LOCATION_AJAX_FORM_ROUTE:string = "location_ajax_form";
export var LOCATION_AJAX_DELETE_ROUTE:string = "location_ajax_delete";
export var LOCATION_AJAX_SAVE_ROUTE:string = "location_ajax_save";
export var LOCATION_AJAX_TABLE_ROUTE:string = "location_ajax_table";
export var LOCATION_AJAX_JSON_ROUTE:string = "location_ajax_json";
export var LOCATION_AJAX_IMAGE_UPLOAD:string = "location_ajax_image_upload";

export interface LocationMarker {
    id:string
    name:string;
    latitude:number;
    longitude:number;
    foodType:number;
    locationType:number;
}