/// <reference path="../modules/references.d.ts" />

import net = require("src/net");

export class DataModel {

    private dataTable:JQuery;
    private contextMenu:JQuery;
    private actionHandler;
    private selectedCallback:(id:string)=>any;
    private reloadRoute:string;
    private loaderIndicator:JQuery;
    private selectedId:string;

    constructor() {
        this.loaderIndicator = $(".table-ajax-loader");
        this.actionHandler = {};
        this.contextMenu = $("#contextMenu");
        this.setTableBindings();
        this.loaderIndicator.hide();
    }

    private setTableBindings():void{
        var self = this;

        var body = $("body");
        body.on("contextmenu", ".data-table tbody tr", function (e) {
            self.contextMenu.css({
                display: "block",
                left: e.pageX,
                top: e.pageY
            });
            self.selectedId = $(this).data("id");

            return false;
        });

        body.click(()=> {
            this.contextMenu.hide();
        });

        this.contextMenu.on("click", "a", function () {
            var action = $(this).data("action");
            self.contextMenu.hide();
            self.actionHandler[action](self.selectedId);
            return false;
        });

        this.dataTable = $(".data-table");

        this.dataTable.find("tbody tr").click(function () {
            self.selectedCallback($(this).data("id"));
        });

        this.dataTable.dataTable({
            iDisplayLength: 100
        });
    }

    public reloadModel():void{
        this.dataTable.toggle();
        this.loaderIndicator.toggle();
        net.Ajax.getHtml(this.reloadRoute, (html:string)=>{
            this.dataTable.parent().html(html);
            this.dataTable.toggle();
            this.loaderIndicator.toggle();
            this.setTableBindings();
        });
    }

    set ReloadRoute(route:string) {
        this.reloadRoute = route;
    }

    public registerAction(action:string, callback:(id:string)=>any):void {
        this.actionHandler[action] = callback;
    }

    public onSelected(callback:(id:string)=>any):void {
        this.selectedCallback = callback;
    }

}
