/// <reference path="../modules/references.d.ts" />

import loc = require("src/location");
import net = require("src/net");

export class ResourceHelper {

    public static baseUrl = "/bundles/hipveganmapadmin";

    public static image(name:string):string {
        return ResourceHelper.baseUrl + "/images/" + name;
    }

}

export class LocationMarkerHelper {

    public static icon(location:loc.LocationMarker):string {
        var ext = ".png";

        var foodType = "";
        var locationType = "";

        switch (location.locationType) {
            case loc.LocationType.CAFE:
                locationType = "cafe";
                break;
            case loc.LocationType.STORE:
                locationType = "store";
                break;
            case loc.LocationType.RESTAURANT:
                locationType = "restaurant";
                break;
            default:
                locationType = "notFound";
                break;
        }

        if (locationType == "")
            console.log(location);

        switch (location.foodType) {
            case loc.FoodType.VEGETARIAN:
                foodType = "vegetarian";
                break;
            case loc.FoodType.OMNIVORE:
                foodType = "mixed";
                break;
            case loc.FoodType.VEGAN:
                foodType = "vegan";
                break;
            default:
                foodType = "notFound";
                break;
        }

        return ResourceHelper.image(locationType + "_" + foodType + ext);
    }

}

export class FileManager {

    public static isImage(file:File):boolean {
        switch (file.type) {
            case "image/jpeg":
            case "image/jpg":
            case "image/png":
                return true;
            default:
                break;
        }
        return false;
    }

    /**
     * Uploads a file which is placed on slide.
     */
    public static uploadFile(url:string, file:File, json:any, onUploaded:(response:string)=>any, onProgress:(progress:number, loaded:number)=>any):void {
        var formData:FormData = new FormData();
        formData.append("file", file);
        formData.append("json", JSON.stringify(json));

        net.Ajax.postFormData(url, formData,
            (e:ProgressEvent) => {
                var progress:number = Math.round(100 * (e.loaded / e.total));
                onProgress(progress, e.loaded);
            },
            onUploaded
        );
    }

}
