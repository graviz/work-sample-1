/// <reference path="modules/references.d.ts" />

import forms = require('src/forms');
import net = require('src/net');
import location = require('src/location');
import gmaps = require('src/gmaps');
import dialog = require('src/dialog');
import model = require('src/model');
import controllers = require('src/controllers');

export class App {

    private locationForm:forms.LocationForm;
    private locationModel:model.DataModel;

    constructor() {
        dialog.Helper.init();

        this.locationModel = new model.DataModel();
        this.locationForm = new forms.LocationForm(this.locationModel);

        dialog.LocationDialog.locationForm = this.locationForm;

        $("#createLocationBtn").click(()=> {
            this.locationForm.create(null);
        });

        this.locationModel.ReloadRoute = Routing.generate(location.LOCATION_AJAX_TABLE_ROUTE);

        this.locationModel.registerAction("detail", (id:string)=> {
            dialog.LocationDialog.detail(id);
        });

        this.locationModel.registerAction("edit", (id:string)=> {
            dialog.LocationDialog.edit(id);
        });

        this.locationModel.registerAction("delete", (id:string)=> {
            this.deleteLocation(id);
        });

        this.locationModel.onSelected((id)=> {
            dialog.LocationDialog.detail(id);
        });

        var controller = new controllers.DashboardController();
        controller.index();
    }


    private deleteLocation(id:string):void {
        var ok = confirm("Are you sure you want to delete this entry?");

        if (ok) {
            var url = Routing.generate(location.LOCATION_AJAX_DELETE_ROUTE, {id: id});
            net.Ajax.getHtml(url, ()=> {
                dialog.Helper.close();
                this.locationModel.reloadModel();
            });
        }
    }


}