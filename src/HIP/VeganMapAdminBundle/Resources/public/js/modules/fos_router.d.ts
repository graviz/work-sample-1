declare class Routing {
    public static generate(route_id:string, params?:Object, absolute?:boolean):string;
}