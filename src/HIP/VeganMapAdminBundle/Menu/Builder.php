<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapAdminBundle\Menu;

use HIP\VeganMapCoreBundle\Common\ServiceHelper;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class Builder extends ContainerAware {

    /** @var Request */
    private $request;

    public function mainMenu(FactoryInterface $factory, array $options) {
        $this->request = ServiceHelper::request();

        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes([
            'class' => 'nav',
            'id' => 'side-nav',
        ]);

        $this->homeItem($menu);
        $this->locationItem($menu);

        return $menu;
    }

    private function homeItem(ItemInterface $parent) {
        $item = $this->addItem($parent, 'menu.dashboard', 'admin_index', [], 'fa-home');

        if (in_array($this->request->get('_route'), ['admin_index']))
            $item->setCurrent(true);
    }

    private function locationItem(ItemInterface $parent) {
        $item = $this->addItem($parent, 'menu.locations', 'location_index', [], 'fa-globe');

        if (in_array($this->request->get('_route'), ['location_index']))
            $item->setCurrent(true);
    }

    private function addItem(ItemInterface &$menu, $name, $route = '', $routeParams = [], $icon = null) {
        $name = $translator = $this->container->get('translator')->trans($name);

        return $menu->addChild($name, [
            'route' => $route,
            'routeParameters' => $routeParams,
            'extras' => ['safe_label' => true, 'icon' => $icon],
        ]);
    }

}