<?php
/**
 * @author Stefan Beier
 */

namespace HIP\VeganMapAdminBundle\Form;

use HIP\VeganMapCoreBundle\FoodType;
use HIP\VeganMapCoreBundle\LocationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocationFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', 'text', [
                'label' => 'location.name'
            ])->add('description', 'textarea', [
                'label' => 'location.description'
            ])
            ->add('address', 'text', [
                'label' => 'location.address_field'
            ])
            ->add('latitude', 'text', [
                'label' => 'location.lat'
            ])
            ->add('longitude', 'text', [
                'label' => 'location.lng'
            ])
            ->add('telephone', 'text', [
                'label' => 'location.telephone',
                'required' => false
            ])
            ->add('url', 'text', [
                'label' => 'location.url',
                'required' => false
            ])
            ->add('opening_hours', 'collection', [
                'type' => new OpeningHourType(),
                'label' => 'location.opening_hours',
                'cascade_validation' => true
            ])
            ->add('food_type', 'choice', [
                'label' => 'location.food_type',
                'choices' => [
                    FoodType::VEGAN => 'food_type_0',
                    FoodType::VEGETARIAN => 'food_type_1',
                    FoodType::OMNIVORE => 'food_type_2',
                ]
            ])
            ->add('location_type', 'choice', [
                'label' => 'location.location_type',
                'choices' => [
                    LocationType::RESTAURANT => 'location_type_0',
                    LocationType::CAFE => 'location_type_1',
                    LocationType::STORE => 'location_type_2',
                ]
            ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName() {
        return 'location';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'HIP\VeganMapCoreBundle\Document\Location',
            'cascade_validation' => true,
        ));
    }

}